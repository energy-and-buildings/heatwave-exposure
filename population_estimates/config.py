from pathlib import Path

DATA_SRC = Path('~/Data/').expanduser()
WEATHER_SRC = Path('~/Shared/Data/weather').expanduser()
POP_DATA_SRC = DATA_SRC / 'lancet' / 'population'
