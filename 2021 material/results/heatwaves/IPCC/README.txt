
country_mean_heatwave_person_day_IPCC.csv : Means for 1986-2005 and 2016-2020 of heatwave exposure in person-days 

country_mean_heatwave_day_exposure_normalised_IPCC.csv : Means for 1986-2005 and 2016-2020 of the heatwave exposure per person over 65. This is calculated as the total exposure in person-days divided by the population over 65, giving a value in number of heatwave days