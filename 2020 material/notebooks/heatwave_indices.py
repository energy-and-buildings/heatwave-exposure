import numpy as np
import xarray as xr

# NOTE: didn't use Numba for the loops because we rely heavily on 
# 2D boolean indexing, which is not really supported for Numba.
# To use it, would have to rewrite everything as tight loops,
# effectively re-implementing Numpy functionality.
# Could think of storing things as sparse data, but numba doesn't support either
# In the end, we are only iterating over 365 days per year so it really isn't worth the
# effort: better to just run in parallel over years.

# NOTE: did I also try cythonizing this? TBH you are wasting more time
# thinking about optimising than you will ever save!

def heatwaves_counts(dataset_year, reference, days_threshold=3):
    """
    Accepts data as a (time, lat, lon) shaped boolean array.
    Iterates through the array in the time dimension comparing the current
    time slice to the previous one. For each cell, determines whether the
    cell is True (i.e. is over the heatwave thresholds) and whether this is
    the start, continuation, or end of a sequence of heatwave conditions.
    Accumulates the number of occurances and counts the total occurances.
    """
    dataset_year = dataset_year.fillna(-9999)
    dataset_year_asbool = xr.ufuncs.isfinite(dataset_year.where(dataset_year > reference)).values
    
    # Init arrays, pre allocate to (hopefully) improve performance.
    out_shape: tuple = dataset_year_asbool.shape[1:]

    last_slice: bool[:,:] = dataset_year_asbool[0, :, :]
    curr_slice: bool[:,:] = dataset_year_asbool[0, :, :]    
    hw_ends: bool[:,:] = np.zeros(out_shape, dtype=bool)
    mask: bool[:,:] = np.zeros(out_shape, dtype=bool)
    
    # Init as int32 - value will never be > 365
    accumulator = np.zeros(dataset_year_asbool.shape[1:], dtype=np.int32)
    counter = np.zeros(dataset_year_asbool.shape[1:], dtype=np.int32)
    
    for i in range(1, dataset_year_asbool.shape[0]):
        last_slice = dataset_year_asbool[i-1, :, :]
        curr_slice = dataset_year_asbool[i, :, :]

        # Add to the sequence length counter at all positions
        # above threshold at prev time step using boolean indexing
        accumulator[last_slice] += 1
        
        # End of sequence is where prev is true and current is false
        # True where prev and not current
        # Use pre-allicocated arrays for results
        np.logical_and(last_slice, np.logical_not(curr_slice), out=hw_ends)
        np.logical_and(hw_ends, (accumulator > days_threshold), out=mask)

        # Add 1 where the sequences are ending and are > 3
        counter[mask] += 1
        # Reset the accumulator where current slice is empty
        accumulator[np.logical_not(curr_slice)] = 0
    
    # Finally, 'close' the heatwaves that are ongoing at the end of the year
    # End of sequence is where last value of iteration is true and accumulator is over given length
    np.logical_and(curr_slice, (accumulator > days_threshold), out=mask)
    
    # Add the length of the accumulator where the sequences are ending and are > 3
    counter[mask] += accumulator[mask]  

    # Convert np array to xr DataArray
    counter = xr.DataArray(counter, 
                            coords=[dataset_year.latitude.values,
                                    dataset_year.longitude.values,
                                   ], 
                           dims=['latitude', 'longitude'],
                           name='heatwave_count'
                          )
    
    return counter


def heatwaves_days(dataset_year, reference, days_threshold: int=3):
    """
    Accepts data as a (time, lat, lon) shaped boolean array.
    Iterates through the array in the time dimension comparing the current
    time slice to the previous one. For each cell, determines whether the
    cell is True (i.e. is over the heatwave thresholds) and whether this is
    the start, continuation, or end of a sequence of heatwave conditions.
    Accumulates the number of days and counts the total lengths.
    """
    dataset_year = dataset_year.fillna(-9999)
    dataset_year_asbool = (dataset_year > reference).values
    
    # pre allocate arrays
    out_shape: tuple = dataset_year_asbool.shape[1:]

    last_slice: bool[:,:] = dataset_year_asbool[0, :, :]
    curr_slice: bool[:,:] = dataset_year_asbool[0, :, :]    
    hw_ends: bool[:,:] = np.zeros(out_shape, dtype=bool)
    mask: bool[:,:] = np.zeros(out_shape, dtype=bool)
    
    # Init as int32 - value will never be > 365
    accumulator = np.zeros(out_shape, dtype=np.int32)
    days = np.zeros(out_shape, dtype=np.int32)
    
    
    for i in range(1, dataset_year_asbool.shape[0]):
        last_slice = dataset_year_asbool[i-1, :, :]
        curr_slice = dataset_year_asbool[i, :, :]

        # Add to the sequence length counter at all positions
        # above threshold at prev time step using boolean indexing
        accumulator[last_slice] += 1
        
        # End of sequence is where prev is true and current is false
        # True where prev and not current
        # Use pre-allicocated arrays for results
        np.logical_and(last_slice, np.logical_not(curr_slice), out=hw_ends)
        np.logical_and(hw_ends, (accumulator > days_threshold), out=mask)

        # Add the length of the accumulator where the sequences are ending and are > 3
        days[mask] += accumulator[mask]
        # Reset the accumulator where current slice is empty
        accumulator[np.logical_not(curr_slice)] = 0
    
    # Finally, 'close' the heatwaves that are ongoing at the end of the year
    # End of sequence is where last value of iteration is true and accumulator is over given length
    np.logical_and(curr_slice, (accumulator > days_threshold), out=mask)
    
    # Add the length of the accumulator where the sequences are ending and are > 3
    days[mask] += accumulator[mask]
    
    # Convert np array to xr DataArray
    days = xr.DataArray(days, 
                            coords=[dataset_year.latitude.values,
                                    dataset_year.longitude.values,
                                   ], 
                           dims=['latitude', 'longitude'],
                           name='heatwave_length'
                          )
    
    return days


def heatwaves_degree_days(dataset_year, reference, days_threshold: int=3):
    """
    Accepts data as a (time, lat, lon) shaped boolean array.
    Iterates through the array in the time dimension comparing the current
    time slice to the previous one. For each cell, determines whether the
    cell is True (i.e. is over the heatwave thresholds) and whether this is
    the start, continuation, or end of a sequence of heatwave conditions.
    Accumulates the number of days and counts the total lengths.
    """
    dataset_year = dataset_year.fillna(-9999)
    dataset_year_asbool: bool[:,:,:] = (dataset_year > reference).values
    
    # Pre allocate arrays
    out_shape: tuple = dataset_year_asbool.shape[1:]

    last_slice: bool[:,:] = dataset_year_asbool[0, :, :]
    curr_slice: bool[:,:] = dataset_year_asbool[0, :, :]    
    hw_ends: bool[:,:] = np.zeros(out_shape, dtype=bool)
    mask: bool[:,:] = np.zeros(out_shape, dtype=bool)
    
    accumulator = np.zeros(out_shape, dtype=np.int32)
    dd_accumulator = np.zeros(out_shape, dtype=np.float32)
    degree_days = np.zeros(out_shape, dtype=np.float32)
    
    
    len_: int = dataset_year_asbool.shape[0]
    for i in range(1, len_):
        last_slice = dataset_year_asbool[i-1, :, :]
        curr_slice = dataset_year_asbool[i, :, :]
        
        vals_slice = dataset_year.values[i-1, :,: ]

        # Add to the sequence length counter at all positions
        # above threshold at prev time step using boolean indexing
        # Needed to keep track of HW length
        accumulator[last_slice] += 1

        # Add to the accumulator the difference between the actual temp and the threshold
        # Note should aways by > 0 when using the boolean indexing
        dd_accumulator[last_slice] += vals_slice[last_slice] - reference.values[last_slice]
        
        # End of sequence is where prev is true and current is false
        # True where prev and not current
        # Use pre-allicocated arrays for results
        np.logical_and(last_slice, np.logical_not(curr_slice), out=hw_ends)
        # Fill the mask
        np.logical_and(hw_ends, (accumulator > days_threshold), out=mask)

        # Add the length of the accumulator where the sequences are ending and are > 3
        degree_days[mask] += dd_accumulator[mask]
        
        # Reset the accumulator where current slice is empty
        accumulator[np.logical_not(curr_slice)] = 0
        dd_accumulator[np.logical_not(curr_slice)] = 0
        
    # Finally, 'close' the heatwaves that are ongoing at the end of the year
    # End of sequence is where last value of iteration is true and accumulator is over given length
    np.logical_and(curr_slice, (accumulator > days_threshold), out=mask)
    
    # Add the length of the accumulator where the sequences are ending and are > 3
    degree_days[mask] += dd_accumulator[mask]
        
    degree_days = xr.DataArray(degree_days, 
                            coords=[dataset_year.latitude.values,
                                    dataset_year.longitude.values,
                                   ], 
                           dims=['latitude', 'longitude'],
                           name='heatwave_degree_day'
                          )    
    return degree_days



def heatwaves_degrees(dataset_year, reference, days_threshold=3):
    """
    Accepts data as a (time, lat, lon) shaped boolean array.
    Iterates through the array in the time dimension comparing the current
    time slice to the previous one. For each cell, determines whether the
    cell is True (i.e. is over the heatwave thresholds) and whether this is
    the start, continuation, or end of a sequence of heatwave conditions.
    Accumulates the number of days and counts the total lengths.
    """
    dataset_year = dataset_year.fillna(-9999)
    dataset_year_asbool = (dataset_year > reference).values
    
    # Pre allocate arrays
    last_slice = dataset_year_asbool[0, :, :]
    
    accumulator = np.zeros(dataset_year_asbool.shape[1:], dtype=np.int32)
    dd_accumulator = np.zeros(dataset_year_asbool.shape[1:], dtype=np.float32)
    days = np.zeros(dataset_year_asbool.shape[1:], dtype=np.int32)
    
    degree_days = np.zeros(dataset_year_asbool.shape[1:], dtype=np.float32)
    
    hw_ends = np.zeros(dataset_year_asbool.shape[1:], dtype=bool)
    
    mask = np.zeros(dataset_year_asbool.shape[1:], dtype=bool)
    
    
    for i in range(1, dataset_year_asbool.shape[0]):
        last_slice = dataset_year_asbool[i-1, :, :]
        data_slice = dataset_year_asbool[i, :, :]
        
        vals_slice = dataset_year.values[i-1, :,: ]

        # Add to the sequence length counter at all positions
        # above threshold at prev time step using boolean indexing
        # Needed to keep track of HW length
        accumulator[last_slice] += 1

        # Add to the accumulator the difference between the actual temp and the threshold
        # Note should aways by > 0 when using the boolean indexing
        dd_accumulator[last_slice] += vals_slice[last_slice] - reference.values[last_slice]
        
        # End of sequence is where prev is true and current is false
        # True where prev and not current
        # Use pre-allicocated arrays for results
        np.logical_and(last_slice, np.logical_not(data_slice), out=hw_ends)
        # Fill the mask
        np.logical_and(hw_ends, (accumulator > days_threshold), out=mask)

        # Add the length of the accumulator where the sequences are ending and are > 3
        degree_days[mask] += dd_accumulator[mask]
        days[mask] += accumulator[mask]
        
        # Reset the accumulator where current slice is empty
        accumulator[np.logical_not(data_slice)] = 0
        dd_accumulator[np.logical_not(data_slice)] = 0
        
    degrees = degree_days / days
    degrees = xr.DataArray(degrees, 
                            coords=[dataset_year.latitude.values,
                                    dataset_year.longitude.values,
                                   ], 
                           dims=['latitude', 'longitude'],
                           name='heatwave_degree'
                          )    
    return degrees