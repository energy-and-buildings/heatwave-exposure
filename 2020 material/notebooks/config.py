from pathlib import Path

DATA_SRC = Path('~/Data/').expanduser()
WEATHER_SRC = DATA_SRC / 'weather'
POP_DATA_SRC = DATA_SRC / 'lancet' / 'population'
