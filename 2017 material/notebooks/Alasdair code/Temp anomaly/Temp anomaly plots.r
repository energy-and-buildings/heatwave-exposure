### Plots of the exposure to annual mean temperature change ###

setwd("XXXX")

## Files for the plotmap function ##
source("libfig.r")
source("rclim.r")


## Load temperature and population data ##

load("annual.summer.anomaly.rcp26.RData") ## array of the model mean temperature anomaly
mean.tas.26 <- mean.tas.rcp26                    ## for each year.
load("annual.summer.anomaly.rcp85.RData") 
mean.tas.85 <- mean.tas                      
                 

load("regridded_population_ssp2.RData") ## population projections
ssp2 <- population.regridded

load("regridded_population_ssp1.RData") ## population projections
ssp1 <- population.regridded

## Set longitude latitude and load grid cell area (km^2) ## 

lons<- seq(-178.75,178.75,2.5)
lats <- seq(-88.75,88.75,2.5)
load("area2.RData")

####  Calculate the 20 year average anomaly  ####

long.85 <- long.26 <-  near.85 <- near.26 <- matrix(0,nrow=144,ncol=72)  ## near: 2020-2039, long: 2080-2099.


   for(i in 1:length(lons)){
     for (j in 1:length(lats)){
       
       near.85[i,j] <- mean(mean.tas.85[i,j,15:35])  # rcp8.5 2020-2039 anomaly
        long.85[i,j] <- mean(mean.tas.85[i,j,74:93]) # rcp8.5 2080-2099 anomaly
   
       near.26[i,j] <- mean(mean.tas.26[i,j,15:35]) # rcp2.6 2020-2039
       long.26[i,j] <- mean(mean.tas.26[i,j,74:93]) # rcp2.6 2090-2099


     }
   } 

##

flip1 = near.85[1:72,]
flip2 = near.85[73:144,]       #sort longitudes so that they 
near.85 <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75

flip1 = long.85[1:72,]
flip2 = long.85[73:144,]       #sort longitudes so that they 
long.85 <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75

flip1 = near.26[1:72,]
flip2 = near.26[73:144,]       #sort longitudes so that they 
near.26 <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75

flip1 = long.26[1:72,]
flip2 = long.26[73:144,]       #sort longitudes so that they 
long.26 <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75

####  Impact plots: temp anomaly * people per km sq^2

near.impact.85 <- near.85*(ssp2[,,21]/area2)  ## temp anomaly * population in 2030
long.impact.85 <- long.85*(ssp2[,,81]/area2)  ## temp anomaly * population in 2090

near.impact.26 <- near.26*(ssp1[,,21]/area2)  ## temp anomaly * population in 2030
long.impact.26 <- long.26*(ssp1[,,81]/area2)  ## temp anomaly * population in 2090


brks=c(50,100,250,500,1000,5000,10000)
nbrks = length(brks)

plotmap(lons,lats,near.impact.85,main="",n=nbrks-1,
          breaks=brks,colours=rev(heat.colors(length(brks)-1)),
            orientation=c(-90,0,0))

plotmap(lons,lats,long.impact.85,main="",n=nbrks-1,
          breaks=brks,colours=rev(heat.colors(length(brks)-1)),
            orientation=c(-90,0,0))


## Temp anomalyplots  ##

brks=seq(1,12,1)
nbrks = length(brks)

plotmap(lons,lats,long.85,main="",cex.main=1.5,n=nbrks-1,
          breaks=brks,colours=rev(heat.colors(length(brks)-1)), ## Temp 2080-2099 anomaly
              orientation=c(-90,0,0))

plotmap(lons,lats,near.85,main="",cex.main=1.5,n=nbrks-1,
          breaks=brks,colours=rev(heat.colors(length(brks)-1)),  ## Temp 2020-2039 anomaly
           orientation=c(-90,0,0))


### Global exposure time series ### 


global.exposure.85 <- global.exposure.26 <- global.exposure.constant.85 <- global.exposure.constant.26 <- exposure.constant <- exposure <- 0


for(yr in 1:89){

  flip1 = mean.tas.85[1:72,,yr+4]
  flip2 = mean.tas.85[73:144,,yr+4] 
  temp.change.85 <-rbind(flip2,flip1)
  exposure.85=temp.change.85*(ssp2[,,yr]/area2)
  global.exposure.85[yr] <- mean(exposure.85[exposure.85 >0]) # ssp2 population

  exposure.constant.85=temp.change.85*(ssp2[,,1]/area2)
  global.exposure.constant.85[yr] <- mean(exposure.constant.85[exposure.constant.85 >0])  # constant population
   
  flip1 = mean.tas.26[1:72,,yr+4]
  flip2 = mean.tas.26[73:144,,yr+4] 
  temp.change.26 <-rbind(flip2,flip1)
  exposure.26=temp.change.26*(ssp1[,,yr]/area2) 
  global.exposure.26[yr] <- mean(exposure.26[exposure.26 >0]) # ssp1 projection

  exposure.constant.26=temp.change.26*(ssp1[,,1]/area2)  #constant population
  global.exposure.constant.26[yr] <- mean(exposure.constant.26[exposure.constant.26 >0])
   

}


par(mar=c(5.1,5.1,4.1,2.1))
plot(2010:2098,global.exposure.85,type="l",col=2,lwd=2,xlab="Year",cex.lab=1.5,ylab="Global mean exposure (K)")
lines(2010:2098,global.exposure.constant.85,lty=2,lwd=2,col=2)
lines(2010:2098,global.exposure.26,lty=1,lwd=2,col=4)
lines(2010:2098,global.exposure.constant.26,lty=2,lwd=2,col=4)

legend("topleft",legend=c("RCP 8.5 SSP 2", "RCP 8.5 2010 pop", "RCP 2.6 SSP1", "RCP 2.6 2010 pop"),lwd=2,col=c(2,2,4,4),lty=c(1,2,1,2),cex=1.5)





