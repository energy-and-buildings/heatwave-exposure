## Calculating the summer mean  temp ##
## and corresponding labour productivity  from the netcdf files ##

library(ncdf)

# Specify the file paths for the folders containing the NH and SH tas #

setwd("XXXX/rcp85/tas/nh")
dir.tas.nh <- list.files()
nh.files <- normalizePath(dir.tas.nh)

setwd("XXXX/rcp85/tas/nh")
dir.tas.sh <- list.files()
sh.files <- normalizePath(dir.tas.sh)



## Calculate the multi model mean for each summer month ##

globe.tas <- Tref <- array(0,dim=c(144,72,279))

 for(i in 1:length(dir.tas.nh)){



  tas.nh <- open.ncdf(nh.files[i] )         # Load the tas variables for Northern/Southern  hemisphere
  z = get.var.ncdf( tas.nh, "tas")   
  tas.sh <- open.ncdf(sh.files[i] )
  z2 = get.var.ncdf( tas.sh, "tas")  

 
  for( j in 1:279){
 
      z2[,,j] <- z2[,ncol(z2[,,j]):1, j ]  # the latitudes of the SH array require reversing
      globe.tas[,,j] <- cbind(z2[,,j],z[,,j])      # bind the NH/SH into one array
     

  # Calculatate the multi-model mean ###
      Tref[,,j] = Tref[,,j]+globe.tas[,,j]
     
     }


 }






Tref <- (Tref/length(dir.tas.nh))   # Divide Tref  by the number of models
                                     # for the monthly multi-model mean tas.




## Calculate the summer mean (JJA NH/DJF SH) WBT and corresponding labour productivity ##

summer.mean.tas <- array(0,dim=c(144,72,279/3)) 

k <- seq(1,277,3) 
  for(j in 1:93){
    for (lon in 1:144){
      for(lat in 1:72){ 

       summer.mean.tas[lon,lat,j] <- mean(Tref[lon,lat,k[j]:(k[j]+2)]) # take the summer mean by averaging over 3 time steps
   
    }
   }
  }


#  Calculate mean tas (in Kelvin) for 2080-2099 or 2020:2040 #

long <- near <- matrix(0,nrow=144,ncol=72)
 
  for (lon in 1:144){
      
    for(lat in 1:72){ 

     long[lon,lat] <- mean(summer.mean.tas[lon,lat,74:93])
     near[lon,lat] <- mean(summer.mean.tas[lon,lat,15:34])

   }
 }


