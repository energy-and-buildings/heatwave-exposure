library(chron)

## 2010-04-07
## Cambiato round in floor!!

## convert to date and time from cyclone database into chron format
convertdate <- function(ymdh)
  {
    y <- floor(ymdh/1000000)
    m <- floor(ymdh/10000-y*100)
    d <- floor(ymdh/100-y*10000-m*100)
    h <- floor(ymdh-y*1000000-m*10000-d*100)
    dstr <- paste(m,d,y,sep="/")
    tstr <- paste(h,"00:00",sep=":")    
    tstr[ h==24 ] <- "23:59:59"
    ##cat(dstr," ",tstr)
    chron(dates(dstr),times(tstr))
  }


## to select tracks across B from transit database:
## awk '{if($4==00.00) print $0}' Cyclone_tracks/Transits/transit_nh.dat>transit.B.dat

## to select tracks across A from transit database:
## awk '{if($4==-60.00) print $0}' Cyclone_tracks/Transits/transit_nh.dat>transit.A.dat
    

## default is to select cyclones between 1 October of yr and 1 April of yr+1
## (suitable for Mailier's dataset)
selectyear <- function(transit,yr,column=2,startdate=100100,enddate=040100)
  {
    ## construct years
    yr0 <-  yr   *1000000+startdate
    yr1 <- (yr+1)*1000000+enddate
    result <- transit[ ((transit[,column]<=yr1) & (transit[,column]>=yr0)), ]
    result
  }


selectmonth <- function(transit,mn,column=2)
  {
    ## select cyclones between with month equal to mn
    dates <- transit[,column]
    months <- floor((dates-floor(dates/1000000)*1000000)/10000)
    result <- transit[ months==mn, ]
    result
  }

selectday <- function(transit,dy,column=2,yr=0)
  {
    ## select cyclones between with month equal to mn
    dates <- transit[,column]
    days <- floor((dates-floor(dates/10000)*10000)/100)
    result <- transit[ days==dy, ]
    #if (kk==343){ print(days); print(dates); print(result); print(daysxmonth[yr-1949,mn-1]); print(yr); print(mn) }
    result
  }

selectweek <- function(transit,weekbegin,weekend,column=2)
  {
    ## select cyclones between with month equal to mn
    dates <- transit[,column]
    days <- floor((dates-floor(dates/10000)*10000)/100)
    result <- transit[ days==dy, ]
    result
  }

