## Calculating the summer mean Wet bulb globe temp ##
## and corresponding labour productivity  from the netcdf files ##

library(ncdf)

# Specify the file paths for NH and SH tas, hurs and psl folders #

setwd("XXXX/rcp85/tas/nh")
dir.tas.nh <- list.files()
nh.files <- normalizePath(dir.tas.nh)

setwd("XXXX/rcp85/tas/sh")
dir.tas.sh <- list.files()
sh.files <- normalizePath(dir.tas.sh)


setwd("XXXX/rcp85/hurs/nh")
dir.hurs.nh <- list.files()
nh.hurs.files <- normalizePath(dir.hurs.nh)

setwd("XXXX/rcp85/hurs/sh")
dir.hurs.sh <- list.files()
sh.hurs.files <- normalizePath(dir.hurs.sh)

setwd("XXXX/rcp85/psl/nh")
dir.psl.nh <- list.files()
nh.psl.files <- normalizePath(dir.psl.nh)

setwd("XXXX/rcp85/psl/sh")
dir.psl.sh <- list.files()
sh.psl.files <- normalizePath(dir.psl.sh)



## Calculate the wet bulb temperature for each summer month ##

WBT <- WBGT <- globe.tas <- globe.psl <- globe.hurs <- array(0,dim=c(144,72,279))



 for(i in 1:length(dir.tas.nh)){



  tas.nh <- open.ncdf(nh.files[i] )         # Load the tas variables for Northern/Southern  hemisphere
  z = get.var.ncdf( tas.nh, "tas")   
  tas.sh <- open.ncdf(sh.files[i] )
  z2 = get.var.ncdf( tas.sh, "tas")  

  hurs.nh <- open.ncdf(nh.hurs.files[i] )   # '' hurs ''
  z3 = get.var.ncdf( hurs.nh, "hurs")   
  hurs.sh <- open.ncdf(sh.hurs.files[i] )
  z4 = get.var.ncdf( hurs.sh, "hurs")   

  psl.nh <- open.ncdf(nh.psl.files[i] )  # '' ps ''
  z5 = get.var.ncdf( psl.nh, "psl")   
  psl.sh <- open.ncdf(sh.psl.files[i] )
  z6 = get.var.ncdf( psl.sh, "psl")  


  for( j in 1:279){   # 279 is the number of time steps; 3 months a year * 93 years
    
      z2[,,j] <- z2[,ncol(z2[,,j]):1, j ]  # the latitudes of the SH files require reversing
      globe.tas[,,j] <- cbind(z2[,,j],z[,,j])      # bind the NH/SH into one file
 
      z4[,,j] <- z4[,ncol(z4[,,j]):1,j ]
      globe.hurs[,,j] <- cbind(z4[,,j],z3[,,j])      

      z6[,,j] <- z6[,ncol(z6[,,j]):1 ,j]     
      globe.psl[,,j] <- cbind(z6[,,j],z5[,,j])   
    

# Calculatate the wetbulb and  wetbulb globe temperature at each time step

      Tref=globe.tas[,,j]
      rhref = globe.hurs[,,j]
      p=globe.psl[,,j]/100
      esat=exp((-2991.2729/Tref^2) - (6017.0128/Tref) +18.87643854 -0.028354721*Tref + (1.7838301E-5)*Tref^2 -(8.4150417E-10)*Tref^3 + (4.4412543E-13)*Tref^4 +2.858487*log(Tref))/100
      wsat = 621.97*esat/(p-esat)
      w=rhref/100* wsat
      TL=1/(1/(Tref-55)-log(rhref/100)/2840)+55
      TE=Tref*(1000/p)^(0.2854*(1-0.28E-3*w)) *exp((3.376/TL-0.00254) *w* (1+0.81E-3* w))
      WBT[,,j]= WBT[,,j]+(45.114-51.489* (TE/273.15)^(-3.504))
      WBGT[,,j]= WBGT[,,j]+0.7*(45.114-51.489* (TE/273.15)^(-3.504))+0.3*(Tref-273.15)     
  
    #  WBGT is the array of the sum of all model wet bulb globe temperatures at each time step
  

     }


 }






WBGT <- (WBGT/length(dir.tas.nh))   # Divide the sum of the wet bulb globe temperatures 
                                     # by the number of models for the mean at each time step.




## Calculate the summer mean (JJA NH/DJF SH) WBT and corresponding labour productivity ##

summer.mean.WBGT <- lab.prod <- array(0,dim=c(144,72,279/3)) 

k <- seq(1,277,3) 
  for(j in 1:93){
    for (lon in 1:144){
      for(lat in 1:72){ 

       summer.mean.WBGT[lon,lat,j] <- mean(WBGT[lon,lat,k[j]:(k[j]+2)]) # take the summer mean by averaging over 3 time steps
       lab.prod[lon,lat,j] <- 100 - 25*max(0,(summer.mean.WBGT[lon,lat,j])-25)^(2/3)
   
    }
   }
  }

