### Plots of the exposure to annual mean temperature change ###

setwd("XXXXX")

## Files for the plotmap function ##
source("libfig.r")
source("rclim.r")



## Load temperature and population data ##


load("WBGT.rcp85.timeseries.Rdata") ## model mean wet bulb globe temp

load("labour_capacity_rcp85.RData") ## array of the multi-model mean annual labour capacity, 2006-2100 (%) 

load("historic.capacity.RData")  ## the historic summer mean labour capacity, 1986-2005 (%)
 
load("rural_population_ssp2.RData")   ## ssp2 rural population
ssp2.rural <- rural.regridded

## Longitudes and latitudes and cell area (km^2) ## 

lons<- seq(-178.75,178.75,2.5)
lats <- seq(-88.75,88.75,2.5)
load("area2.RData")

####  Calculate 20 year mean labour capacity  ####

long <-  near <- matrix(0,nrow=144,ncol=72)  ## near: 2020-2039, long: 2080-2099.


   for(i in 1:length(lons)){
     for (j in 1:length(lats)){
       
       near[i,j] <- mean(lab.prod[i,j,15:35])
        long[i,j] <- mean(lab.prod[i,j,74:93])

     }
   } 

flip1 = long[1:72,]
flip2 = long[73:144,]       #sort longitudes so that they 
long <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75



flip1 = historic.capacity[1:72,]
flip2 = historic.capacity[73:144,]       #sort longitudes so that they match population
historic <-rbind(flip2,flip1)



####  Lost labour: years of labour lost annualy per km^2: (100-lab.prod)/4 * people in grid cell

current.impact.rural <- ((100-historic)/(4*100))*ssp2.rural[,,1]/area2  ## Man-years labour lost annually
                                                                        ## at each grid cell in 2010
impact.rural.2010 <- ((100-long)/(4*100))*(ssp2.rural[,,1]/area2)  ## Man years labour lost in 2090 (rcp8.5)
                                                                   ## with 2010 population.

impact.rural.2090 <- ((100-long)/(4*100))*(ssp2.rural[,,81]/area2) ## Man years labour lost in 2090 (rcp8.5)
                                                                   ## with 2090 population.


## plots of man years labour lost at each grid cell
brks=c(1,5,10,25,50,100,150,200,300,500)
nbrks = length(brks)

plotmap(lons,lats,current.impact.rural,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))
plotmap(lons,lats,impact.rural.2010,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))
plotmap(lons,lats,impact.rural.2090,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))


###  Change in labour lost under rcp8.5 from 2010  ###

brks=c(1,5,10,25,50,75,100,150,300)
nbrks = length(brks)
change.ssp2 <- impact.rural.2090 - current.impact.rural  # climate + population change
change.2010 <- impact.rural.2010 - current.impact.rural  # only climate change

plotmap(lons,lats,change.ssp2,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))
plotmap(lons,lats,change.2010,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))


### Global total labour lost - time series plot ### 


long <-  near <- matrix(0,nrow=144,ncol=72)  ## near: 2020-2039, long: 2080-2099.

historic.loss <- ((100-historic)/(4*100))*ssp2.rural[,,1] 
historic.loss[is.nan(historic.loss)] <- 0 
global.exposure.ssp2 <- global.exposure.2010 <- 0

for(yr in 1:89){
      

  flip1 = lab.prod[1:72,,yr+4]
  flip2 = lab.prod[73:144,,yr+4]       #sort longitudes so that they 
  lab.prod.flip <-rbind(flip2,flip1)   #  match the population data:  -178.75,178.75
  lab.prod.flip[is.nan(lab.prod.flip)] <- 0
  labour.lost2 <- ((100-lab.prod.flip)/(4*100))*ssp2.rural[,,yr] -historic.loss## average annual years labour lost long term
  labour.lost2.current <- ((100-lab.prod.flip)/(4*100))*ssp2.rural[,,1] -historic.loss## average annual years labour lost long term

  global.exposure.ssp2[yr] = sum(labour.lost2[ssp2.rural[,,yr] > 0])
  global.exposure.2010[yr] = sum(labour.lost2.current[ssp2.rural[,,1] > 0])


}
   

par(mar=c(5.1,5.1,4.1,2.1))

plot(c(2010,seq(2019,2099,10)),global.exposure.ssp2[c(1,9,19,29,39,49,59,69,79,89)],cex.lab=1.5, xlab="Year",ylab="Years labour lost annually",,ylim=c(30000000,300000000),type="l",col=2,lwd=2)

lines(2011:2099,global.exposure.2010,lwd=2)

legend("topleft",legend=c("SSP2", "2010"),lwd=2,col=c(2,1),cex=1.5)
