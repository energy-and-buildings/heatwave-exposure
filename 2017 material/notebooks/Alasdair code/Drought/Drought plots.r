### Drought plots  ###

setwd("F:/Impact 4 Drought")
## Files for the plotmap function ##
source("libfig.r")
source("rclim.r")



## Load temperature and population data #


load("regridded_population_ssp2.RData") ## SSP3 population projections
ssp2 <- population.regridded

load("droughts.rcp85.Rdata") # multi model mean number of drought events
                             # 20 year average (2010-2029)-(2080-2099)

## Longitudes and latitudes and cell area (km^2) ## 

lons<- seq(-178.75,178.75,2.5)
lats <- seq(-88.75,88.75,2.5)
load("area2.RData")


### Plots ###


# 2020-2039 average annual number of drought events "near" #
flip1 = droughts.rcp85[1:72,,30]
flip2 = droughts.rcp85[73:144,,30]       #sort longitudes so that they 
near <-rbind(flip2,flip1)       #  match the population data:  -178.75,178.75

#2080-2099 average annual number of drought events "long" #
flip1 = droughts.rcp85[1:72,,70]
flip2 = droughts.rcp85[73:144,,70]       
long <-rbind(flip2,flip1)   


## Impact plots: population per km^2 * number of drought events ##
current.impact <- 0.1*(ssp2[,,1]/area2) # current impact is 0.1 (1 in 10 year event) 
                                        # * population per km^2

impact.2010 <- long*(ssp2[,,1]/area2)  ##  2080-2099 climate change * 2010 population
impact.2090 <- long*(ssp2[,,80]/area2)  ## 2080-2099 climate change * population in 2090

change.2010 <- impact.2010-current.impact     ## change in exposure 2010 population
change.2090 <- impact.2090 - current.impact   ## change in exposure ssp2 2090 population

nbrks = length(brks)
brks=c(10,25,50,75,100,150,200,275)

plotmap(lons,lats,change.2010,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))
plotmap(lons,lats,change.2090,main="",n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0))

## Change in drought frequency ##
change.droughts <- (droughts.rcp85[,,70])/0.1  # projected probability of a drought event
                                               # divided by the historic probability

flip1 = change.droughts[1:72,]
flip2 = change.droughts[73:144,]       #sort longitudes so that they 
change.droughts2 <-rbind(flip2,flip1)

brks=c(0.1,0.3,0.7,0.9,1.1,1.5,2,3,5,10)
nbrks = length(brks)
cols <- bluered(seq(0,nbrks-2,by=1), "linear",white=3)

plotmap(lons,lats,change.droughts2,main="",n=nbrks-1,breaks=brks,colours=cols,orientation=c(-90,0,0))


##Global mean time series ##

current.impact <- 0.1*(ssp2[,,1]) 
impact.const <- impact.change <- 0

for(yr in 1:70){

  flip1 = droughts.rcp85[1:72,,yr]
  flip2 = droughts.rcp85[73:144,,yr]       #sort longitudes so that they 
  drought.index <-rbind(flip2,flip1)  

  change.exposure <- (drought.index*(ssp2[,,1]))- current.impact 
  change.exposure2 <- (drought.index*(ssp2[,,yr+10]))- current.impact

  impact.const[yr] <- sum(change.exposure[ssp2[,,1] > 0])
  impact.change[yr] <- sum(change.exposure2[ssp2[,,yr+10] > 0])

}

par(mar=c(5.1,5.1,4.1,2.1))

plot(2020:2089,impact.change,ylim=c(200000000,1400000000),cex.lab=1.5,xlab="Year",ylab="Change in number of people at risk",type="l", col=2,lwd=2)
lines(2020:2089,impact.const,lwd=2)
legend("topleft",legend=c("SSP2", "2010"),lwd=2,col=c(2,1),cex=1.5)
