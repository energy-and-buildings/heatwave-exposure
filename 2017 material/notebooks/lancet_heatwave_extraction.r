library(ncdf)
library(evd)
setwd("historical/day/tasmin")

setwd("historical/day/tasmin/nh")
dir.tas.nh <- list.files()
nh.files <- normalizePath(dir.tas.nh)

setwd("historical/day/tasmin/sh")
dir.tas.sh <- list.files()
sh.files <- normalizePath(dir.tas.sh)

setwd("rcp85/day/tasmin_2020_2040/nh")
dir.tas.nh.future <- list.files()
nh.files.future <- normalizePath(dir.tas.nh.future)

setwd("rcp85/day/tasmin_2020_2040/sh")
dir.tas.sh.future <- list.files()
sh.files.future <- normalizePath(dir.tas.sh.future)

heatwave.nh.historic.length <- heatwave.sh.historic.length <- heatwave.nh.future.length <- heatwave.sh.future.length  <- matrix(0,nrow=144,ncol=36)
heatwave.nh.historic.duration <- heatwave.sh.historic.duration <-  heatwave.nh.future.duration <- heatwave.sh.future.duration <- matrix(0,nrow=144,ncol=36)


nh.percentile <- sh.percentile <- sh.percentile.future <- nh.percentile.future <- matrix(0,nrow=144,ncol=36)
global.historic.heatwaves.length <- global.future.heatwaves.length <- global.historic.heatwaves.duration <- global.future.heatwaves.duration <-array(0,dim=c(144,72,33))

for(i in 1:length(dir.tas.sh)){

tasmin.nh <- open.ncdf(nh.files[i] )
z = get.var.ncdf( tasmin.nh, "tasmin")   # variable
tasmin.sh <- open.ncdf(sh.files[i] )
z2 = get.var.ncdf( tasmin.sh, "tasmin")   # variable

#lon11 <- get.var.ncdf( tasmin.sh.future, "time")  

tasmin.nh.future <- open.ncdf(nh.files.future[i] )
z3 = get.var.ncdf( tasmin.nh.future, "tasmin")   # variable
tasmin.sh.future <- open.ncdf(sh.files.future[i] )
z4 = get.var.ncdf( tasmin.sh.future, "tasmin")   # variable

  for (lon in 1:144){
        for(lat in 1:36){ 
   
   # Definition 1: the mean temperature + 5 degrees, exceeded for 5 consecutive days
   # Definition 2: the temperature exceeds the 99th percentile for 3 consecutive days
   u <- mean(c(z[lon,lat,]))+5  # NH  heatwave thresholds for definition 1
   u2 <-  mean(c(z2[lon,lat,]))+5 # SH heatwave thresholds for definition 1

     heatwave1 <- c(unlist(lapply(clusters(c(z[lon,lat,]),u,keep.names=F), function(x) length(x))))
     heatwave2 <- c(unlist(lapply(clusters(c(z2[lon,lat,]),u2,keep.names=F), function(x) length(x))))
     heatwave3 <- c(unlist(lapply(clusters(c(z3[lon,lat,]),u,keep.names=F), function(x) length(x))))
     heatwave4 <- c(unlist(lapply(clusters(c(z4[lon,lat,]),u2,keep.names=F), function(x) length(x))))

     heatwave.nh.historic.length[lon,lat] <- length(heatwave1[heatwave1 >5])      ## total number of heatwave events, 
     heatwave.sh.historic.length[lon,lat] <- length(heatwave2[heatwave2 > 5])      ## total number of heatwave events, 
 
     heatwave.nh.future.length[lon,lat] <- length(heatwave3[heatwave3 > 5])
     heatwave.sh.future.length[lon,lat] <- length(heatwave4[heatwave4 >5])      ## total number of heatwave events, 

   }
  }
  heatwave.sh.historic.length <- heatwave.sh.historic.length[,ncol(heatwave.sh.historic.length):1]
  global.historic.heatwaves.length[,,i] <- cbind(heatwave.sh.historic.length ,heatwave.nh.historic.length)

  heatwave.sh.future.length <- heatwave.sh.future.length[,ncol(heatwave.sh.future.length):1]
  global.future.heatwaves.length[,,i] <- cbind(heatwave.sh.future.length ,heatwave.nh.future.length)

}

mean.duration1 <- (global.future.heatwaves.duration/33)/(global.future.heatwaves.length/33)
mean.duration2 <- (global.historic.heatwaves.duration/33)/(global.historic.heatwaves.length/33)


change <- global.future.heatwaves.length-global.historic.heatwaves.length

 mean.hwd <- matrix(0,nrow=144,ncol=72)

for (lon in 1:144){
        for(lat in 1:72){ 
    mean.hwd[lon,lat] <- mean(change[lon,lat,])
    }
  }

#Bind the NH and SH into a single array
flip1 = mean.hwd[1:72,]
flip2 = mean.hwd[73:144,]
mean.hwd2 <-rbind(flip2,flip1) 


#Plot
brks <- seq(-5,90,5)
nbrks = length(brks)
main=" Change in number of heatwaves"
plotmap(lons,lats,mean.hwd2,main=main,"",cex.main=1.5,n=nbrks-1,breaks=brks,colours=rev(heat.colors(length(brks)-1)),orientation=c(-90,0,0),colorbar=T)




