from pathlib import Path as _Path

DATA_SRC = _Path('~/Data/').expanduser()
WEATHER_SRC =  _Path('~/Shared/Data/weather').expanduser()
POP_DATA_SRC = DATA_SRC / 'lancet' / 'population'
