#!/usr/bin/env python
# coding: utf-8

# # The temperature quantiles


from pathlib import Path

import numpy as np
import xarray as xr

import dask
from dask.diagnostics import ProgressBar

def year_from_filename(name):
    return int(name.split('_')[-1][:4])

# WEATHER_SRC =  Path('~/Shared/Data/weather').expanduser()
WEATHER_SRC =  Path('~/').expanduser()


# In[2]:


MAX_YEAR = 2021
REFERENCE_YEAR_START = 1986
REFERENCE_YEAR_END = 2005

TEMPERATURES_FOLDER = WEATHER_SRC / 'era5_land' /'era5_land_daily_summary'

QUANTILES = [0.90, 0.95, 0.99]

CLIMATOLOGY_QUANTILES_FOLDER = WEATHER_SRC / 'era5_land' / 'era5_land_daily_quantiles' 


def main(t_var):
    file_list = []
    for folder in sorted([p for p in TEMPERATURES_FOLDER.glob('*') 
            if int(p.name) >= REFERENCE_YEAR_START
            and int(p.name) <= REFERENCE_YEAR_END
           ]):
        file_list += [p for p in folder.rglob('*.nc') if t_var in p.name]


    # with dask.config.set(**{'array.slicing.split_large_chunks': False}):
    daily_temperatures = xr.open_mfdataset(file_list, combine='by_coords', chunks ={'latitude': 100, 'longitude': 100})
    daily_temperatures = daily_temperatures.chunk({'time': -1})


    daily_quantiles = daily_temperatures.quantile(QUANTILES, dim='time')
    print('Processing')
    CLIMATOLOGY_QUANTILES = (CLIMATOLOGY_QUANTILES_FOLDER / 
                         f'daily_{t_var}_quantiles_{"_".join([str(int(100*q)) for q in QUANTILES])}_1986-2005.nc')
    with dask.config.set(scheduler='processes'):
        daily_quantiles = daily_quantiles.compute()
        daily_quantiles.to_netcdf(CLIMATOLOGY_QUANTILES)

if __name__ == '__main__':
    t_var = 'tmin'

    main(t_var)





