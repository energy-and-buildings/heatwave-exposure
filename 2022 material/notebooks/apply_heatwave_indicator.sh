#!/bin/sh

# Use job array to apply to all years

#SBATCH --array=1980-2021
#SBATCH --partition=public-gpu
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=0:15:00

# load Anaconda, this will provide Jupyter as well.
module load Anaconda3
export XDG_RUNTIME_DIR=""

# srun ~/.conda/envs/lancet/bin/python ~/'Documents/Lancet/2022 material/notebooks/2.1 Apply heatwave indicator.py' $1
srun ~/.conda/envs/lancet/bin/python ~/'Documents/Lancet/2022 material/notebooks/2.1 Apply heatwave indicator.py' heatwave_days
srun ~/.conda/envs/lancet/bin/python ~/'Documents/Lancet/2022 material/notebooks/2.1 Apply heatwave indicator.py' heatwave_counts
wait