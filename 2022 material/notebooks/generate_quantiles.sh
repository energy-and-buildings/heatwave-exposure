#!/bin/sh

#SBATCH --partition=shared-cpu
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=36
#SBATCH --time=10:00:00
#SBATCH --mem-per-cpu=2000

# load Anaconda, this will provide Jupyter as well.
module load Anaconda3
export XDG_RUNTIME_DIR=""

# launch Jupyter notebook
# srun python ~/'Documents/Lancet/2022 material/notebooks/2.0 Calculate temperature quantiles.py'
# ~/.conda/envs/lancet/bin/python ~/'Documents/Lancet/2022 material/notebooks/2.0 Calculate temperature quantiles.py'
srun ~/.conda/envs/lancet/bin/python ~/'Documents/Lancet/2022 material/notebooks/2.0 Calculate temperature quantiles.py'