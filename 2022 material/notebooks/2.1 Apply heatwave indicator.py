#!/usr/bin/env python
# coding: utf-8

# # Calculate heatwave occurrances 
# 
# ## NEW heatwave definition 2021
# 
# Heatwaves are now defined as:
# 
# Tmin > 95percentile AND Tmax > 95percentile
# 
# For more than 2 consecutive days (i.e. total of 3 or more days).
# 
# This replaces the definition of only Tmin > 99percentile for more than 3 consecutive days (total of 4 or more days).
# 
# This is what is requested from the Lancet. To be honest it's not clear whether this produces a substantially 'better' indicator since all heatwave indicators are arbitrary in absence of covariate data (i.e. impact data). Furthermore we know that the health impacts are mediated by many other things, so in any case we are truely interested just in the trends i.e. demonstrating that there is a) more heatwaves and b) more exposure to heatwaves - this can be followed by local studies but (as always) the point is to present a general risk factor trend.
# 


from pathlib import Path

import numpy as np
import xarray as xr

WEATHER_SRC =  Path('~/').expanduser()
DATA_SRC =  Path('~/').expanduser()

import heatwave_indices

xr.set_options(keep_attrs=True)


# ### Setup Paths
# 
# > NOTE: considered just adding the newest year each time instead of re-calculating the whole thing. HOWEVER in reality, the input data is still changing year to year, so far have needed to re-calculate anyway (e.g. change in resolution, change from ERAI to ERA5, in the future probably use ERA5-Land, etc). Although it seems like a cool idea to have a reproducible method where each year you just add one thing, in practice its better to have one 'frozen' output corresponding to each publication, so that it's easy to go back later to find data corresponding to specific results. Additionally, generating one file per year means you have a folder full of files that are harder to share, and the outputs are in the end pretty small (<50MB in Float32)}.


MAX_YEAR = 2021

TEMPERATURES_FOLDER = WEATHER_SRC / 'era5_land' / 'era5_land_daily_summary'
CLIMATOLOGY_QUANTILES = WEATHER_SRC / 'era5_land' / 'era5_land_daily_quantiles'


INTERMEDIATE_RESULTS_FOLDER = DATA_SRC / 'lancet'/ 'heatwaves'/ 'results_2022'
INTERMEDIATE_RESULTS_FOLDER.mkdir(exist_ok=True)

assert INTERMEDIATE_RESULTS_FOLDER.is_dir()


# Apply heatwave index function to selected vars using selected threshold days

def ds_for_year(year):
    temperature_files = []
    for folder in sorted([p for p in TEMPERATURES_FOLDER.glob('*') 
            if int(p.name) == year
           ]):
        temperature_files += [p for p in folder.rglob('*.nc')]
    ds = xr.open_mfdataset(temperature_files)
    ds = ds.drop('time_bnds')
    ds = ds.transpose('time','latitude','longitude')
    return ds
    

def apply_func_for_file(func, year, t_thresholds, t_var_names, days_threshold=2):
    ds = ds_for_year(year)
    
    datasets_year = [ds[name] for name in t_var_names]
    result = func(datasets_year, t_thresholds, days_threshold)
    
    # Add a year dimension matching the input file
    result = result.expand_dims(dim={'year': [year]})
    return year, result


def apply_func_and_save(func, year, output_folder, t_thresholds,  t_var_names=['tmin', 'tmax'], 
                        days_threshold=2, overwrite=False,
                        filename_pattern='indicator_{year}.nc'
                       ):
    output_file = output_folder / filename_pattern.format(year=year)
    if output_file.exists() is False and overwrite is False:
        year, result = apply_func_for_file(func, year, t_thresholds, t_var_names=t_var_names, days_threshold=days_threshold)
        result.to_netcdf(output_file)
        return f'Created {output_file}'
    else:
        return f'Skipped {output_file}, already exists'


# # Calculate heatwave occurances

def main(indicator, year):
    if indicator == 'heatwave_counts':
        func = heatwave_indices.heatwaves_counts_multi_threshold
    elif indicator == 'heatwave_days':
        func = heatwave_indices.heatwaves_days_multi_threshold
    else:
        raise RuntimeError('Wrong indicator name')

    out_folder = INTERMEDIATE_RESULTS_FOLDER / indicator
    out_folder.mkdir(exist_ok=True)
    
    quantiles_files = list(CLIMATOLOGY_QUANTILES.rglob('*.nc'))


    # ## Load ERA5 reference temperature quantiles
    # Load both the tmin and tmax quatiles and place in a list
    QUANTILE = 0.95

    t_quantiles = xr.open_mfdataset(quantiles_files, compat='override')
    # Need to use tolerance/nearest b/c of floating point drift (0.95 != 0.95)
    t_min_threshold = t_quantiles.tmin.sel(quantile=QUANTILE, drop=True, tolerance=0.001, method='nearest')
    t_max_threshold = t_quantiles.tmax.sel(quantile=QUANTILE, drop=True, tolerance=0.001, method='nearest')

    t_thresholds = [t_min_threshold, t_max_threshold]

    return apply_func_and_save(func, year, out_folder, t_thresholds, t_var_names=['tmin', 'tmax'])

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('indicator')
    parser.add_argument('--year', type=int)
    args = parser.parse_args()
    if not args.year:
        import os
        year = int(os.getenv('SLURM_ARRAY_TASK_ID'))
    else:
        year = args.year
    
    if year is None:
        raise RuntimeError('Must supply year as arg or env var')
    
    from datetime import datetime
    now = datetime.now()
    print(now.isoformat(), f' processing {args.indicator} {year}')
    res = main(args.indicator, year)
    now = datetime.now()
    print(now.isoformat(), f' {args.indicator} {year}: {res}')
    